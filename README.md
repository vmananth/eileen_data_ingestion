## Data ingestion scripts for NSF EAGER grant

Scripts for retrieving, cleaning, and creating data from multiple repositories.

The structure of the repository should be as follows:

```
/nsf_data_ingestion
 /nber
 /arxiv
 /medline
```

```
# Package Structure - 
/nsf_data_ingestion
  # Airflow Package 
  /airflow
  # Source Systems - (Each System consists of Download and Parquet Write Process Scripts)
  /arxiv
  /medline
  /federal_reporter
  /pubmed
  # Config - (Config Files)
  /config
    /nsf_config
    /spark_config
  # Objects - (Params particular to Source System)
  /objects
    /data_source_params
  # Utils - (Common Functions and Utilities)
  /utils
    /utils_functions
  # Models - (Models used by recommendation System - Aggregation)
  /models
    /tfdif_aggregate  
 ```
 Additional Systems can be added by including parameters in config file such as paths, urls etc....
 Include setters in data__source_params to reflect these config parameters.
 Access data_source_params from the scripts by passing source system as argument.
  
 # Airflow
  Airflow folder conatins dags for scheduling these processes.
  Include Tasks or Seperate Dags.

 # Libraries Folder
  The libraries folder contains libraries that are required to be passed to the worker nodes.
   


# Airflow

## Details -

Airflow is a DAG Scheduler that is setup for the Eileen Recommendation Search Engine.

It serves 2 purpose -

- Updating the Data Sets for the Engine from various sources.
- Automation Checks.
- Scheduling the automatic Elastic Search Index refresh.

Directory - /home/ananth/airflow/

Dags - /home/ananth/airflow/dags/

Data - /home/ananth/airflow/ (medline/arxiv/pubmed/federal)

Data and HDFS details - config module of nsf contain this information.

Topic Model HDFS location - '/user/ananth/tfidf_topic/'

Provision to add new users -

https://airflow.apache.org/security.html

Running the Server -

- airflow scheduler ( this is the scheduler that keeps track of the tasks to be run)
- airflow webserver -p 8082 ( web server for managing DAGS)

Source Code - https://github.com/rgvananth/eileen_data_ingestion

Branch - feature/nsf_new_workflow

Directory Structure -

- libraries (set of packages zipped for the worker nodes)

- nsf_data_ingestion (main source package)

  - airflow (airflow package containing dag scripts)

  - config (config for airflow, processing, HDFS, Spark Processing)

  - objects (the setter module for various sources)

  - arxiv (represents a source, download and processing scripts)

  - federal_reporter (represents a source, download and processing scripts)

  - medline (represents a source, download and processing scripts)

  - pubmed (represents a source, download and processing scripts)

  - models (spark processing of publications into models)

  - kimun_loader (ingest the data generated by models to elastic server)

  - utils (utilities and functions)

- requirements.txt (Initial python environment setup)

- setup .py (script to set up the zip package of nsf_data_ingestion)

- run_setup.sh (create a attributable egg file for spark loading)

## Completed

- [x] Airflow Setup
- [x] Setup Data Processing for 4 Sources using Airflow
- [x] Modify Pubmed Parser to parse HDFS files.
- [x] Topic Modeling using Airflow
- [x] End to End Airflow System and Elastic Search
- [x] Investigate Microsoft Academic Graph Data set
- [x] Move Airflow to Home directory, making it feasible for anyone in group to run and monitor it.
- [x] Modify the ARXIV data set download (It takes too long, nearly 3 days. Modify script to download only latest publications.
- [x] Add Microsoft Academic Graph Data set.
- [x] Add open citations to data sets.
- [x] Add semantic scholar to data sets.
- [x] Expose a Rest API on Eileen back-end to switch Elastic Index from Airflow for timely updates on index with 0 downtime.

## In Progress

- [ ] Code cleanup in certain modules.

## Screenshot

![image.png](https://storage.googleapis.com/slite-api-files-production/files/e9f7308a-5304-470b-8645-c3556e6529af/image.png)



# Elastic Search

## Details -

Eileen Recommendation Search Engine uses Elastic Search as it back-end data source.

Elastic Search Server Details -

Server - http://xxxxxxxx:9201/

Index - kimun_version3

Mapping - Mapping file on Slite.

Kibana Server Details -

Server - http://xxxxxxxx:8080

Elastic Search has 2 components -

- Index (this is similar to a database name)
- Mapping (this is a schema for the database itself)

Mapping -

We First setup up the mapper which is like a schema for our index. In other words this would be an empty database with field names. Run the Mapping file on the Dev Tools Console of Kibana.

We can perform the Elastic Loading in many ways -

- Logstash (convert the airflow generated model data into a csv and load)
- Hadoop-ES connector (leverage spark to push the data !!)

We use the second approach as its much faster pushing data to a server in parallel with a predefined mapping.

Analyzers -

Analyzers the way of looking up a document. Each field can different analyzers.

2 types of analyzers -

- Index Analyzer (the way you want to index a par ticular field - stop words, bigram, tokenizer)
- Search Analyzer (the way you look up a index during run time search)

Having different analyzer for different fields help match the document details in many ways and boost their importance which is ideal for a recommendation engine.

## Completed

- [x] Load data from Airflow to Elastic Search
- [x] Setup Analyzers for different fields
- [x] Better Search Results
- [x] Better Recommendations
- [x] Use Different analyzers for better search results.
- [x] Eliminate Special characters from title field in search result.
- [x] Certain documents do not have Date and Venue ( Possible change required in pubmed parser or kimun_front-end. Needs investigation)


Reading Material -

https://logz.io/blog/elasticsearch-mapping/

[[https://logz.io/blog/elasticsearch-mapping/]](https://logz.io/blog/elasticsearch-mapping/)
