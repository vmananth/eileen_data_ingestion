from setuptools import setup, find_packages

setup(name='nsf_data_ingestion',
      version = '0.0.1',
      description = 'nsf_data_ingest',
      packages = find_packages(),
      zip_safe = False)
